# based off of https://raw.githubusercontent.com/docker-library/golang/master/1.14/alpine3.11/Dockerfile

FROM alpine:3.11

ARG ORIGIN=https://go.googlesource.com/go
ARG BASE=81df5e69fc189af44459a4c6520b1c99d0210a92
ARG CL=187317
ARG REV=16

ENV GOLANG_VERSION=1.14.3 \
	GOPATH=/go \
	GO2PATH=/go \
	PATH=$GOPATH/bin:/usr/local/go/bin:$PATH

WORKDIR $GOPATH

RUN apk add --no-cache \
		ca-certificates

# set up nsswitch.conf for Go's "netgo" implementation
# - https://github.com/golang/go/blob/go1.9.1/src/net/conf.go#L194-L275
# - docker run --rm debian:stretch grep '^hosts:' /etc/nsswitch.conf
RUN [ ! -e /etc/nsswitch.conf ] && echo 'hosts: files dns' > /etc/nsswitch.conf

COPY ./build-go2go /tmp/

RUN set -eux; \
	apk add --no-cache --virtual .build-deps \
		bash \
		gcc \
		musl-dev \
		openssl \
		go \
		git \
	; \
	export \
		GOOS="$(go env GOOS)" \
		GOARCH="$(go env GOARCH)" \
		GOHOSTOS="$(go env GOHOSTOS)" \
		GOHOSTARCH="$(go env GOHOSTARCH)" \
	; \
# also explicitly set GO386 and GOARM if appropriate
# https://github.com/docker-library/golang/issues/184
	apkArch="$(apk --print-arch)"; \
	case "$apkArch" in \
		armhf) export GOARM='6' ;; \
		armv7) export GOARM='7' ;; \
		x86) export GO386='387' ;; \
	esac; \
	\
	git config --global user.name nobody; \
	git config --global user.email "nobody@nowhere.com"; \
	GO2GO_DEST=/usr/local/go /tmp/build-go2go; \
	\
	rm -rf \
    /tmp/build-go2go \
    /usr/local/go/.git \
# https://github.com/golang/go/blob/0b30cf534a03618162d3015c8705dd2231e34703/src/cmd/dist/buildtool.go#L121-L125
		/usr/local/go/pkg/bootstrap \
# https://golang.org/cl/82095
# https://github.com/golang/build/blob/e3fe1605c30f6a3fd136b561569933312ede8782/cmd/release/releaselet.go#L56
		/usr/local/go/pkg/obj \
	; \
	apk del .build-deps; \
	export PATH="/usr/local/go/bin:$PATH"; \
	mkdir -p "$GOPATH/src" "$GOPATH/bin" && chmod -R 777 "$GOPATH"; \
	go version
