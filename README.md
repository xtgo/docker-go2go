# go2go

Prototype code sourced from https://go-review.googlesource.com/c/go/+/187317/

## Links

- Blog Post: https://blog.golang.org/why-generics
- Overview: https://go.googlesource.com/proposal/+/master/design/go2draft-generics-overview.md
- Proposal: https://github.com/golang/proposal/blob/master/design/go2draft-contracts.md
- Source: https://gitlab.com/xtgo/docker-go2go/
- Docker: https://hub.docker.com/r/xtgo/go2go
- Online Playground: https://ccbrown.github.io/wasm-go-playground/experimental/generics/

Design/Meetup Feedback: https://forms.gle/FXpX2TrVkbR2EMC8A

## Setup

### Docker

For use with docker, run:

```sh
docker run --rm -it xtgo/go2go
```

### On-Host

This assumes an existing Go installation is present on your system.

For on-host installation on a unix-like system, run:

```sh
./build-go2go
```

Within the current directory, this will produce:
go2goroot and src directories.
go2goroot is a go installation containing the generics prototype,
and src contains example generics code files.

It will also print export commands that can be copied into a shell
in order to set appropriate PATH and GO2PATH variables.

## Usage

```sh
cd src/gsort
go2go test
```

*or*

```sh
go2go test gsort
```
